class HomepageController < ApplicationController
  def index
    # Setting up model for the Contact Form
    @contact = Contact.new
  end

  def download
    send_file 'downloads/Daniel_Gerhab_Resume.pdf', type: 'application/pdf', disposition: 'attachment'
  end

  def send_contact
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if @contact.deliver # delivers if is not a spam and is valid
      flash.now[:notice] = 'Thank you for your message. I will read it!'
    else
      flash.now[:alert] = 'Cannot send message.'
    end
    render :index
  end
end
