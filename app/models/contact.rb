class Contact < MailForm::Base
  attribute :name, validate: true
  attribute :phone
  attribute :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message
  # Nickname must be a blank field, indicated by captcha, used to prevent spam
  attribute :nickname, captcha: true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Portfolio - #{name} is contacting me",
      :to => "djgerhab@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
end
