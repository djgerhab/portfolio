$(document).ready(function(){

  // This is to prevent older versions of js from erroring out on code since the newer versions are less strict
  "use strict";

  /* =================================
  WOW ANIMATION - http://mynameismatthieu.com/WOW/
  =================================== */

  const wow = new WOW({
    boxClass:     'wow',       // animated element css class (default is wow)
    animateClass: 'animated',  // animation css class (default is animated)
    offset:       50,          // distance to the element when triggering the animation (default is 0)
    mobile:       false        // trigger animations on mobile devices (true is default)
  });
  wow.init();

  /* =================================
  VERTICLE TEXT
  =================================== */

  let current = 1;
  let height = $('.roles').height();
  const numberDivs = $('.roles').children().length;
  const first = $('.roles div:nth-child(1)');
  setInterval(function() {
    const number = current * -height;
    first.css('margin-top', number + 'px');
    if (current === numberDivs) {
      first.css('margin-top', '0px');
      current = 1;
    } else current++;
  }, 2000);

  /* =================================
  NAVBAR
  =================================== */

  // Sticky Bar
  $(window).scroll(function () {
    const top = $(document).scrollTop();
    const threshold = 200;
    const navbar = $('.main-menu');

    if (top > threshold) {
      navbar.addClass('tiny');
      navbar.css('opacity','1');
    } else {
      navbar.removeClass('tiny');
    }
  });

  // This prevents continued focus on links after navigating away
  $('.navbar a').mouseup(function(){
    $(this).blur();
  });

  /* =================================
  WAYPOINT
  =================================== */

  // Handles the scrolling
  const waypoint = $('.waypoint').waypoint({
    handler: function(direction) {
      const dataslide = $(this.element).attr('data-slide');
      const navbtn = $('.navbar-nav li[data-slide="' + dataslide + '"]');
      if (direction === 'down') {
        navbtn.addClass('active').prev().removeClass('active');
      }
      else {
        navbtn.addClass('active').next().removeClass('active');
      }
    },
    offset: 90 // Indicates 100px offset from the top is when the trigger occurs
  });

  // Handles the navbar links
  $('.navbar-nav li, .navbar-brand').click(function (e) {
    const dataslide = $(this).attr('data-slide');
    if (typeof dataslide !== typeof undefined && dataslide !== false) {
      window.location.hash = $(this).children('a').attr('rel');
      e.preventDefault();
      goToByScroll(dataslide);
    }
  });

  function goToByScroll(slide) {
    // Due to the stickybar, the top calculations are off and will present a different display
    // depending on if the stickybar is on top or not
    // TODO: Fix this bug, currently just applying extra offset for all
    const stickybarOffset = 90;
    $('html, body').animate({
      scrollTop: $('.section[data-slide="' + slide + '"]').offset().top - stickybarOffset
    }, 1000);
  }

});
