/* =================================
Google Maps
=================================== */

// Initialize and add the map
function initMap() {
    const location = {lat: 40.256, lng: -75.464};
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center: location,
    });
    const marker = new google.maps.Marker({
        position: location,
        map: map,
    });
}