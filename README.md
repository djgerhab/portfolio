# Daniel Gerhab's Portfolio Website

## About

This is the git repo that that contains the codebase for the site www.danielgerhab.com.

It is using Ruby (v2.6.5) on Rails (v6.1). I enjoy using the language, the asset pipeline, and the structure that it provides.

Please feel free to look through the codebase and comment on any questions or suggestions

Thanks for visiting!

## Startup
`bundle install`

`rails server`

## Env Variables
For local development, create a .env file with the following variables:

EMAIL_USERNAME=(email)
EMAIL_PASSWORD=(generated app password)
RACK_ENV=development
PORT=3000

## Heroku Setup & Push
Ensure Heroku cli has been installed on computer and login via:
`heroku login`

Then point the existing Heroku app to the correct remote Heroku git URL
`heroku git:remote -a frozen-scrubland-90566`

Deploy code with: `git push heroku main`

## Notes
If manually removing lock file, you will need to run the following to make Heroku bundler happy:
`bundle lock --add-platform x86_64-linux --add-platform ruby`