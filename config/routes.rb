Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  resources :homepage, controller: 'homepage', only: [:index]

  # Download resume
  get 'homepage/resume', to: 'homepage#download'

  # Contact us form
  post 'homepage/contact', to: 'homepage#send_contact'

  # redirect invalid urls to the root
  get '*path' => redirect('/')

  root 'homepage#index'
end
