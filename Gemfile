source 'https://rubygems.org'

# Bootstrap 3 powered by Sass
gem 'bootstrap-sass', '~> 3.3.3'

# Client side validation
gem 'bootstrap-validator-rails'

# Font Awesome powered by Sass
gem 'font-awesome-sass'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Had an issue with this dependency with my version of OS and Ruby, added this fixed it
gem 'json', github: 'flori/json', branch: 'v1.8'

# Allows for emailing straight from a form using ActiveModel
gem 'mail_form'

# Use Postgres as the database for Active Record
gem 'pg'

# Use Puma as the app server
gem 'puma'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Loads enviromental variables through the .env file. For production we use heroku config
  gem 'dotenv-rails'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen'

  # This gem replaces the standard Rails error page with a much better and more useful error page.
  gem 'better_errors'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Ruby version, Heroku wants to know this
ruby '2.6.5'
